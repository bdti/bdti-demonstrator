# Prediction & Informed Decision-making

## Description
This folder contains the necessary documents and guidelines to create the second part of the BDTI demonstrator on the BDTI platform, 2. Prediction & Informed Decision-making. Open the markdown file 'User guide - Prediction & Informed Decision-making' and follow the steps mentioned to create this part of the demonstrator.