# Smart Data Ingestion

## Description
This folder contains the necessary documents and guidelines to create the first part of the BDTI demonstrator on the BDTI platform, 1. Smart Data Ingestion. Open the markdown file 'User guide A - Smart Data Ingestion' and follow the steps mentioned to create this part of the demonstrator.