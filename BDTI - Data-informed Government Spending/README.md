# BDTI Data-informed Government Spending

## Description
This folder contains the necessary documents and guidelines to recreate the BDTI demonstrator on the BDTI platform. The demonstrator is made up of two parts that can be built seperately:
1. Smart Data Ingestion
2. Prediction & Informed Decision-making

 Open one of the folders with these names to learn more.