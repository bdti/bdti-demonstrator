Copyright 2023 European Union

Documentation in this repository is licensed under the Creative Commons Attribution 4.0 License, and code samples are licensed under the BSD 3-Clause licence.

![Alt text](BDTI_Banner_generic.png "BDTI banner")


# Big Data Test Infrastructure (BDTI) Demonstrator

The [Big Data Test Infrastructure](https://big-data-test-infrastructure.ec.europa.eu/index_en) (BDTI)
is ready-to-use, free of charge, analytics cloud stack for the public sector offered to all European public administrations to experiment with open-source tools and foster the re-use of public sector data for a data-informed public sector.

Interested in playing with BDTI? For any question or request for support, please visit our [webiste](https://big-data-test-infrastructure.ec.europa.eu/apply-bdti_en) and contact us via EC-BDTI-PILOTS@ec.europa.eu

## Data-informed Goverment Spending - General Introduction

This GitLab repository contains an example use-case developed on the [Big Data Test Infrastructure (BDTI) platform](https://big-data-test-infrastructure.ec.europa.eu/index_en). 





The purpose of the use-case is to demonstrate how the services on the platform can be used and integrated into a single workflow.

There are two parts to the use-case. Together, they follow a story of a civil servant, Elena, digging into the data on the spending on public lighting. In each part, Elena uses different services from the BDTI platform to achieve her goals. Each part of Elena's use-case can be replicated using the User-guide of each part's dedicated folder in this repository. Below, you can read the general storyline of the use-case and see which services are used in each part.

Please check the dedicated section in our [webiste](https://big-data-test-infrastructure.ec.europa.eu/resources/bdti-value-demonstrator_en) amd the [final dashboard](https://superset-1690295135.p1.bdti.dataplatform.tech.ec.europa.eu/superset/dashboard/2/?native_filters_key=Tfzg3YiXUQnCDwoeaZ7tpU-4NQnLAr_CGvq2tcSZy0rb93LfhnjKC3Ye_fn_qvf2).

## Part 1 - Smart Data Ingestion
In the first part of the use case, Elena uses Optical Character Recognition to extract relevant information from a folder of PDF invoices. Her government received these (fabricated) invoices from the Energy Supplier. She then combines this output with other data (.csv, .xlsx) on her government's spending. She feeds the consolidated dataset to a relational database that she can access with her dashboarding service. Elena visualises the enriched government spending data in a dashboard. Analysing the charts, she discovers that her government is spending more on public lighting than comparable municipalities.

Services used: 
- KNIME
- PostgreSQL
- Apache Superset


## Part 2 - Prediction & Informed Decision-making
Elena decides her government should investigate how to save spending (and emissions) by reducing public lighting. She creates a dashboard that policy-makers can use to analyse different scenarios of reduced lighting. The scenarios are simulated using predictive modeling in a processing service. The output of the processing service is again fed to a relational database and a dashboarding tool pulls the data directly from the database.

Services used: 
- JupyterLab
- PostgreSQL
- Apache Superset
